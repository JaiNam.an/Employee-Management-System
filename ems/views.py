# from rest_framework.parsers import JSONParser
from rest_framework.viewsets import ModelViewSet
# from django.http import JsonResponse, HttpResponse
# from django.views.decorators.csrf import csrf_exempt
from .models import Role, Client, Project, Employee, Department, EmployeeDetail
from .serializers import (RoleSerializer, ClientSerializer,
                          ProjectSerializer, EmployeeSerializer,
                          DepartmentSerializer, EDetailSerializer)


class RoleViewSet(ModelViewSet):
    queryset = Role.objects.all()
    serializer_class = RoleSerializer


class ClientViewSet(ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ProjectViewSet(ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class EmployeeViewSet(ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer


class DepartmentViewSet(ModelViewSet):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer


class EDetailViewSet(ModelViewSet):
    queryset = EmployeeDetail.objects.all()
    serializer_class = EDetailSerializer


# @csrf_exempt
# def all_role_view(request):
#     if request.method == 'GET':
#         queryset = Role.objects.all()
#         serializer = RoleSerializer(queryset, many=True)
#         return JsonResponse(serializer.data, safe=False)
#
#     elif request.method == 'POST':
#         data = JSONParser().parse(request)
#         serializer = RoleSerializer(data=data)
#
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data, status=201)
#         return JsonResponse(serializer.errors, status=400)


# @csrf_exempt
# def role_view(request, pk):
#     try:
#         queryset = Role.objects.get(pk=pk)
#     except Role.DoesNotExist:
#         return HttpResponse(status=404)
#
#     if request.method == 'GET':
#         serializer = RoleSerializer(queryset)
#         return JsonResponse(serializer.data)
#
#     elif request.method == 'PUT':
#         data = JSONParser().parse(request)
#         serializer = RoleSerializer(data=data)
#
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data)
#         return JsonResponse(serializer.errors, status=400)
#
#     elif request.method == 'DELETE':
#         queryset.delete()
#         return HttpResponse(status=204)
