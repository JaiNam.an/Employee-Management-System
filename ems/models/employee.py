from django.db import models
from django.core.validators import MinLengthValidator, MaxLengthValidator


class Employee(models.Model):
    employee_id = models.CharField(primary_key=True, max_length=5, unique=True)
    employee_name = models.CharField(max_length=25, null=False)
    contact_no = models.CharField(max_length=10, null=False, unique=True,
                                  validators=[MinLengthValidator(10), MaxLengthValidator(10)])
    salary = models.DecimalField(max_digits=12, decimal_places=2, null=False)
    joining_date = models.DateField('Joining Date', null=False)

    def __str__(self):
        return f'{self.employee_id} - {self.employee_name}'
