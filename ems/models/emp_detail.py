from django.db import models
from ems.models import Role, Project, Employee, Department


class EmployeeDetail(models.Model):
    employee = models.OneToOneField(Employee, on_delete=models.CASCADE, related_name='employee')
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    project = models.ManyToManyField(Project)
    manager = models.ForeignKey(Employee, null=True, on_delete=models.CASCADE, related_name='manager')

    def __str__(self):
        return f"{self.employee}"
