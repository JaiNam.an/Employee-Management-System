from django.db import models


class Role(models.Model):
    role_id = models.CharField(primary_key=True, max_length=5, unique=True)
    role_name = models.CharField(max_length=50, unique=True, null=False)

    def __str__(self):
        return f'{self.role_id} - {self.role_name}'
