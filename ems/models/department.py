from django.db import models


class Department(models.Model):
    department_id = models.CharField(primary_key=True, max_length=5, unique=True)
    department_name = models.CharField(max_length=25, null=False, unique=True)

    def __str__(self):
        return f'{self.department_id} - {self.department_name}'
