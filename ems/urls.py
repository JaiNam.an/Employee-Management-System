from .viewsets import Views
from django.urls import path


app_name = 'ems'
urlpatterns = [
    # path('role/', views.all_role_view, name='role_view'),
    # path('role/<pk>/', views.role_view, name='role_retrieve'),

    path('role/', Views.role_view, name='role'),
    path('role/<pk>/', Views.role_retrieve, name='role_retrieve'),

    path('client/', Views.client_view, name='client'),
    path('client/<pk>/', Views.client_retrieve, name='client_retrieve'),

    path('project/', Views.project_view, name='project'),
    path('project/<pk>/', Views.project_retrieve, name='project_retrieve'),

    path('emp-detail/', Views.edetail_view, name='emp_detail'),
    path('emp-detail/<pk>/', Views.edetail_retrieve, name='emp_detail_retrieve'),

    path('employee/', Views.employee_view, name='employee'),
    path('employee/<pk>/', Views.employee_retrieve, name='employee_retrieve'),

    path('department/', Views.department_view, name='department'),
    path('department/<pk>/', Views.department_retrieve, name='department_retrieve'),
]
