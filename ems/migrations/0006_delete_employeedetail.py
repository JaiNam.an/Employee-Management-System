# Generated by Django 4.1.3 on 2022-11-07 18:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ems', '0005_alter_employeedetail_role'),
    ]

    operations = [
        migrations.DeleteModel(
            name='EmployeeDetail',
        ),
    ]
