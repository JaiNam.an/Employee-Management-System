from django.urls import reverse
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient


URLS = ['department', 'role', 'client', 'project', 'employee', 'emp-detail']


# Create your tests here.
class UserAPITest(TestCase):
    def setUp(self) -> None:
        self.client = APIClient()

    def test_create_department_successful(self):
        data = {
            "department_id": "D101",
            "department_name": "Engineer"
        }
        result = self.client.post(reverse(f'ems:{URLS[0]}'), data)
        self.assertEqual(result.status_code, status.HTTP_201_CREATED)

    def test_create_role_successful(self):
        data = {
            "role_id": "R201",
            "role_name": "Software Developer"
        }
        result = self.client.post(reverse(f'ems:{URLS[1]}'), data)
        self.assertEqual(result.status_code, status.HTTP_201_CREATED)

    def test_create_client_successful(self):
        data = {
            "client_id": "C301",
            "client_name": "Software Developer",
            "contact_no": "8172637129"
        }
        result = self.client.post(reverse(f'ems:{URLS[2]}'), data)
        self.assertEqual(result.status_code, status.HTTP_201_CREATED)

    def test_create_project_successful(self):
        data = {
            "project_id": "P401",
            "project_name": "Library Management System",
            "project_rating": "3",
            "client": self.client.get(f'ems:{URLS[2]}')
        }
        result = self.client.post(reverse(f'ems:{URLS[3]}'), data)
        self.assertEqual(result.status_code, status.HTTP_201_CREATED)
