from django.shortcuts import HttpResponseRedirect


def redirect_page(request):
    return HttpResponseRedirect('ems/department/')
